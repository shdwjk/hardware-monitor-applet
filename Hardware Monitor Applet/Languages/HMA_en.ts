<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../GUI/AboutDialog.ui" line="32"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/AboutDialog.ui" line="49"/>
        <source>Hardware Monitor Applet 3.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/AboutDialog.ui" line="168"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Developed by: JimmyD&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;a href=&quot;http://www.hwinfo.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;HWiNFO&lt;/span&gt;&lt;/a&gt; is a trademark of Martin Malík&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;http://openhardwaremonitor.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;Open Hardware Monitor&lt;/span&gt;&lt;/a&gt; is a trademark of Michael Möller&lt;/p&gt;&lt;p&gt;All other trademarks are the property of their respective owners.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackgroundPage</name>
    <message>
        <location filename="../GUI/BackgroundPage.cpp" line="7"/>
        <location filename="../GUI/BackgroundPage.cpp" line="27"/>
        <source>Background selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/BackgroundPage.cpp" line="104"/>
        <source>Select Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/BackgroundPage.cpp" line="104"/>
        <source>Images (*.png *.jpg)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BackgroundPageWidget</name>
    <message>
        <location filename="../GUI/BackgroundPageWidget.ui" line="14"/>
        <source>Change background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/BackgroundPageWidget.ui" line="37"/>
        <source>Use the default background or select a custom made one. The background must be 320px X 240px.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/BackgroundPageWidget.ui" line="75"/>
        <source>Default background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/BackgroundPageWidget.ui" line="118"/>
        <source>Custom background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/BackgroundPageWidget.ui" line="165"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateScreenWizard</name>
    <message>
        <location filename="../GUI/CreateScreenWizard.cpp" line="33"/>
        <location filename="../GUI/CreateScreenWizard.cpp" line="82"/>
        <source>Screen Wizard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizePage</name>
    <message>
        <location filename="../GUI/CustomizePage.cpp" line="40"/>
        <source>Introduction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizePageLineWidget</name>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="24"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="32"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="37"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="42"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="50"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="57"/>
        <source>Font Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="83"/>
        <source>Text scrolling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageLineWidget.ui" line="93"/>
        <source>Line spacing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizePageWidget</name>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="14"/>
        <source>Customize page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="37"/>
        <source>Customize the lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="62"/>
        <source>General settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="70"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="75"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="80"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="88"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="95"/>
        <source>Font Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="102"/>
        <source>Text scrolling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/CustomizePageWidget.ui" line="112"/>
        <source>Line spacing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataPage</name>
    <message>
        <location filename="../GUI/DataPage.cpp" line="42"/>
        <source>Select data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataPageWidget</name>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="14"/>
        <source>Pick data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="28"/>
        <source>HWiNFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="41"/>
        <location filename="../GUI/DataPageWidget.ui" line="85"/>
        <location filename="../GUI/DataPageWidget.ui" line="251"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="46"/>
        <location filename="../GUI/DataPageWidget.ui" line="90"/>
        <location filename="../GUI/DataPageWidget.ui" line="291"/>
        <source>Hardware</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="51"/>
        <location filename="../GUI/DataPageWidget.ui" line="95"/>
        <location filename="../GUI/DataPageWidget.ui" line="261"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="56"/>
        <location filename="../GUI/DataPageWidget.ui" line="100"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="61"/>
        <location filename="../GUI/DataPageWidget.ui" line="105"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="66"/>
        <location filename="../GUI/DataPageWidget.ui" line="110"/>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="75"/>
        <source>OHM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="137"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="160"/>
        <location filename="../GUI/DataPageWidget.ui" line="271"/>
        <source>Precision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="190"/>
        <location filename="../GUI/DataPageWidget.ui" line="281"/>
        <source>Add unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="216"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="256"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="266"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="276"/>
        <source>Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/DataPageWidget.ui" line="286"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GraphPage</name>
    <message>
        <location filename="../GUI/GraphPage.cpp" line="6"/>
        <location filename="../GUI/GraphPage.cpp" line="27"/>
        <source>Graph settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GraphPageLineWidget</name>
    <message>
        <location filename="../GUI/GraphPageLineWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageLineWidget.ui" line="32"/>
        <location filename="../GUI/GraphPageLineWidget.ui" line="51"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageLineWidget.ui" line="82"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageLineWidget.ui" line="108"/>
        <source>Pick Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GraphPageWidget</name>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="14"/>
        <source>Change Graph settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="37"/>
        <source>Give every dataline a label and color. This will be used in the graph to add to the legend.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="75"/>
        <source>Enable title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="85"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="92"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="129"/>
        <source>Graph range (seconds)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="179"/>
        <source>Auto Y-axis range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="208"/>
        <source>Min range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/GraphPageWidget.ui" line="244"/>
        <source>Max range</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IntroPage</name>
    <message>
        <location filename="../GUI/IntroPage.cpp" line="7"/>
        <location filename="../GUI/IntroPage.cpp" line="21"/>
        <source>Introduction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IntroPageWidget</name>
    <message>
        <location filename="../GUI/IntroPageWidget.ui" line="14"/>
        <source>Intro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/IntroPageWidget.ui" line="37"/>
        <source>This wizard will walk you trought some easy steps for making a rich screen for the Logitech keyboards.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/IntroPageWidget.ui" line="78"/>
        <source>Screen name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LineEditPage</name>
    <message>
        <location filename="../GUI/LineEditPage.cpp" line="7"/>
        <location filename="../GUI/LineEditPage.cpp" line="27"/>
        <source>Insert text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LineEditPageLineWidget</name>
    <message>
        <location filename="../GUI/LineEditPageLineWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageLineWidget.ui" line="20"/>
        <source>Line text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LineEditPageWidget</name>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="14"/>
        <source>Screen  Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="40"/>
        <source>Add text and data to a line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="73"/>
        <source>Add line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="130"/>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="135"/>
        <source>Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="140"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="145"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="150"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="155"/>
        <source>Precision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="160"/>
        <source>Add Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="165"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/LineEditPageWidget.ui" line="170"/>
        <source>Hardware</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainScreenWidget</name>
    <message>
        <location filename="../GUI/MainScreenWidget.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/MainScreenWidget.ui" line="52"/>
        <location filename="../GUI/MainScreenWidget.ui" line="75"/>
        <location filename="../GUI/MainScreenWidget.ui" line="112"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../GUI/mainwindow.cpp" line="65"/>
        <source>No Logitech keyboard found!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.cpp" line="69"/>
        <source>Connected to: Logitech monochrome (G15, G15s, G510) keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.cpp" line="73"/>
        <source>Connected to: Logitech color (G19, G19s) keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.cpp" line="219"/>
        <source>Current Language changed to %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="../GUI/mainwindow.ui" line="14"/>
        <source>Hardware Monitor Applet Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="75"/>
        <source>Order screens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="150"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="157"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="161"/>
        <source>Temperature Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="168"/>
        <source>Autostart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="178"/>
        <location filename="../GUI/mainwindow.ui" line="196"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="185"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="201"/>
        <source>Report a bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="206"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="211"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="216"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="221"/>
        <source>Go to background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="232"/>
        <source>Celsius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="237"/>
        <source>General Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="245"/>
        <source>Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="256"/>
        <source>HWiNFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="264"/>
        <source>Open Hardware Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="269"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="277"/>
        <source>1 s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="285"/>
        <source>2 s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="296"/>
        <source>5 s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="304"/>
        <source>10 s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="315"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/mainwindow.ui" line="323"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OrderWindow</name>
    <message>
        <location filename="../GUI/OrderWindow.ui" line="14"/>
        <source>Order Logitech screens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/OrderWindow.ui" line="34"/>
        <source>Created screens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/OrderWindow.ui" line="93"/>
        <location filename="../GUI/OrderWindow.ui" line="113"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/OrderWindow.ui" line="142"/>
        <source>Main Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/OrderWindow.ui" line="179"/>
        <location filename="../GUI/OrderWindow.ui" line="276"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/OrderWindow.ui" line="199"/>
        <location filename="../GUI/OrderWindow.ui" line="296"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/OrderWindow.ui" line="236"/>
        <source>Sub track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/OrderWindow.ui" line="335"/>
        <location filename="../GUI/OrderWindow.ui" line="355"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Screen/StartScreen.cpp" line="49"/>
        <source>Thanks for using Hardware Monitor Applet.
Open settings to create new screens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Screen/StartScreen.cpp" line="62"/>
        <source>Thanks for using Hardware Monitor Applet. Open settings to create new screens</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenTypePage</name>
    <message>
        <location filename="../GUI/ScreenTypePage.cpp" line="7"/>
        <location filename="../GUI/ScreenTypePage.cpp" line="26"/>
        <source>Type selection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenTypePageWidget</name>
    <message>
        <location filename="../GUI/ScreenTypePageWidget.ui" line="14"/>
        <source>Select screen type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/ScreenTypePageWidget.ui" line="37"/>
        <source>Select a screentype that you want to create.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/ScreenTypePageWidget.ui" line="77"/>
        <source>Normal text screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/ScreenTypePageWidget.ui" line="106"/>
        <source>Graph screen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SortScreenDialog</name>
    <message>
        <location filename="../GUI/SortScreenDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/SortScreenDialog.ui" line="39"/>
        <source>Created screen list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/SortScreenDialog.ui" line="92"/>
        <location filename="../GUI/SortScreenDialog.ui" line="271"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/SortScreenDialog.ui" line="131"/>
        <location filename="../GUI/SortScreenDialog.ui" line="310"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/SortScreenDialog.ui" line="171"/>
        <location filename="../GUI/SortScreenDialog.ui" line="350"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/SortScreenDialog.ui" line="178"/>
        <location filename="../GUI/SortScreenDialog.ui" line="357"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GUI/SortScreenDialog.ui" line="217"/>
        <location filename="../GUI/SortScreenDialog.ui" line="396"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
